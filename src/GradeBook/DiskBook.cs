﻿using System.Collections.Generic;
using System.IO;

namespace GradeBook
{
    public class DiskBook : Book
    {

        public DiskBook(string name) : base(name)    
        {
            
        }

        public override event GradeAddedDelegate GradeAdded;


        public override bool AddGrade(double grade)
        {
            using (var openFile = File.AppendText($"{Name}.txt"))
            {
                openFile.WriteLine(grade.ToString());
                if (GradeAdded != null)
                {
                    GradeAdded(this, new System.EventArgs());
                }
            }

            return true;
        }

        public override Statistics GetStatistics()
        {
            using (var openFile = File.OpenText($"{Name}.txt"))
            {
                var line = openFile.ReadLine();
                var gradeList = new List<double>();

                while (line != null)
                {
                    gradeList.Add(double.Parse(line));
                    line = openFile.ReadLine();
                }
                return new Statistics(gradeList);
            }
        }
    }
}