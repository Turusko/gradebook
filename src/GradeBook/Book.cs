﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GradeBook
{
    public abstract class Book : NamedObject, IBook
    {
        public List<double> grades;

        public Book(string name) : base(name)
        {
            grades = new List<double>();
        }

        public abstract event GradeAddedDelegate GradeAdded;
        public abstract bool AddGrade(double grade);
        public abstract Statistics GetStatistics();

        
    }
}
