﻿using System;

namespace GradeBook
{
    class Program
    {
        static void Main(string[] args)
        {
            var book = new DiskBook("Chris");
            book.GradeAdded += OnGradeAdded;
            AddGradesToBook(book);

        }

        private static void AddGradesToBook(Book book)
        {
            var run = true;

            while (run)
            {
                Console.WriteLine("Please add Grade or Q to quit or R to see Stats");

                var input = Console.ReadLine();

                switch (input)
                {
                    case var i when i == "Q" || input == "q":
                        run = false;
                        break;

                    case var i when i == "R" || input == "r":
                        Program.ShowStatistics(book.GetStatistics());
                        break;

                    default:
                        try
                        {
                            book.AddGrade(double.Parse(input));
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }

                        break;
                }

            }
        }

        public static void ShowStatistics(Statistics stats)
        {
            Console.WriteLine($"Your Result is {stats.Total}");
            Console.WriteLine($"Your average grade is {stats.Average}");
            Console.WriteLine($"Your highest grade is {stats.High}");
            Console.WriteLine($"Your lowest grade is {stats.Low}");
            Console.WriteLine($"Your letter grade is {stats.letter}");
        }

        public static void OnGradeAdded(object s, EventArgs e)
        {
            Console.WriteLine("Grade was added");
        }
    }
}
