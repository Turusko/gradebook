using System.Collections.Generic;

namespace GradeBook
{
    public class Statistics
    {
        public double Total;
        public double High;
        public double Low;
        public double Average;
        public char letter;
        public List<double> Grades { get; set; }

        public Statistics(List<double> grades)
        {
            Grades = grades;
            Total = GradesTotal();
            High = GradesHighest();
            Low = GradesLowest();
            Average = GradesAverage();
            letter = GradesLetter();
        }


        private char GradesLetter()
        {
            var total = GradesTotal();

            switch (total)
            {
                case var x when x >= 90:
                    return 'A';

                case var x when x >= 80:
                    return 'B';

                case var x when x >= 70:
                    return 'C';

                case var x when x >= 60:
                    return 'D';

                default:
                    return 'F';
            }
        }
        private double GradesTotal()
        {
            var result = 0.0;

            foreach (var grade in Grades)
            {
                result += grade;
            }

            return result;
        }
        private double GradesAverage()
        {

            return GradesTotal() / Grades.Count;

        }

        private double GradesHighest()
        {
            var high = double.MinValue;

            foreach (var grade in Grades)
            {
                if (grade > high)
                {
                    high = grade;
                }
            }

            return high;
        }

        private double GradesLowest()
        {
            var low = double.MaxValue;

            foreach (var grade in Grades)
            {
                if (grade < low)
                {
                    low = grade;
                }
            }

            return low;
        }
    }
}