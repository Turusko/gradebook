﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GradeBook
{
    public interface IBook
    {
        event GradeAddedDelegate GradeAdded;

        string Name { get; }
        Statistics GetStatistics();
        bool AddGrade(double grade);
    }
}
