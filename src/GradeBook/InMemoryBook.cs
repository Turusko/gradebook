using System;
using System.Collections.Generic;

namespace GradeBook
{
    public delegate void GradeAddedDelegate(object sender, EventArgs args);

    
    public class InMemoryBook : Book
    {
        
        public override event GradeAddedDelegate GradeAdded;

        public InMemoryBook(string name) : base(name)
        {
            

        }


        public override bool AddGrade(double grade)
        {
            if (grade <= 100 && grade >= 0)
            {
                grades.Add(grade);
                if (GradeAdded != null)
                {
                    GradeAdded(this, new EventArgs());
                }
                return true;
            }
            else
            { 
                Console.WriteLine("Invalid Grade");
                return false;
            }
            
        }

        public override Statistics GetStatistics()
        {
            var stats = new Statistics(grades);

            return stats;
        }

        

    }
}