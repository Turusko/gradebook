using Xunit;
namespace GradeBook.Test
{
    public class LetterGradesTests
    {
        [Theory]
        [InlineData(90, 'A')]
        [InlineData(80, 'B')]
        [InlineData(70, 'C')]
        [InlineData(60, 'D')]
        [InlineData(50, 'F')]
        public void Userisable(int grade, char expectedLetterGrade)
        {
            var book = new Book("Chris");

            book.AddGrade(grade);

            var stats = book.GetStatistics();

            Assert.Equal(expectedLetterGrade, stats.letter);
        }
    }
}