using Xunit;
using System;

namespace GradeBook.Test
{
    public class BookTests
    {
        [Fact]
        public void GradeBookCanBeCreated()
        {
        var book = new Book("chris");

        book.AddGrade(15.34);
        
        book.AddGrade(23.45);

        book.AddGrade(34.22);
        
        var result = book.GetStatistics();

        Assert.IsType<Statistics>(result);

        Assert.Equal(73.01, result.Total, 2);

        Assert.Equal(15.34, result.Low);

        Assert.Equal(34.22, result.High);
        
        Assert.Equal(24.34, result.Average, 2);
        }

        [Fact]
        public void GradeCanNotBeGreaterThanHundread()
        {
            var book = new Book("Chris");
            var result = book.AddGrade(101);
            Assert.False(result);
        }

        [Fact]
        public void GradeCanNotBeLessThanZero()
        {
            var book = new Book("Chris");
            var result = book.AddGrade(-5);
            Assert.False(result);
        }

        [Fact]
        public void GradeCanBeValid()
        {
        var book = new Book("Chris");
            var result = book.AddGrade(5);
            Assert.True(result);
        }

    }

}